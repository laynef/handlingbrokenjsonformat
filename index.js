const fs = require('fs');
const path = require('path');

const root = process.cwd();
const file_path = process.argv[2];
const file_split = file_path.split('/');
const file_name = file_split[file_split.length - 1];

const read_file_str = fs.readFileSync(path.resolve(root, file_path), { encoding: 'utf8' });

const objects_string = read_file_str.split('{').filter(e => /\}/g.test(e)).map(e => {
    return e.split('}')[0];
}).filter(e => {
    return /\"/g.test(e);
}).map(e => `{${e}}`).join(',');

const json_str = `[
    ${objects_string}
]`;

// console.log(json_str)
const json_array = JSON.parse(json_str);

const do_keys_exists = [
    'AVR_Analytics_version',
    'device_utc',
    'user_id',
    'session_id',
    'device_type',
    'device_timezone_offset',
    'Application_version',
    'operating_system_version',
    'session_duration',
];

const lookup = do_keys_exists.reduce((acc, item) => ({ ...acc, [item]: true }), {});

let bool = true;
let object_that_fails = null;
for (let i = 0; i < json_array.length; i++) {
    const keys_exists = [];
    const object = json_array[i];
    if (bool) {
        for (let key in object) {
            if (lookup[key]) {
                const value = object[key];
                keys_exists.push(key);
                if (!value) bool = false;
                if (!value) object_that_fails = JSON.stringify(object, null, 4);
            }

            if (!bool) {
                break;
            }
        }

        if (keys_exists.length !== do_keys_exists.length) bool = false; 
        if (keys_exists.length !== do_keys_exists.length) object_that_fails = JSON.stringify(object, null, 4); 
    } else {
        break;
    }
}

const filenm = file_name.split('.')[0];
const generated_path = path.join(__dirname, 'generated', filenm + '.json');
fs.writeFileSync(generated_path, json_str);

console.log(`Do all keys exist in this file: ${bool}`);
console.log(`Object that failed: ${object_that_fails}`);
console.log(`New json file was created for proper development format in path: ${generated_path}`);

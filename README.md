# Check broken json file

# Installation

- Required Node Version >= 8

Open your terminal in Application

Install homebrew then node with two commands below into your terminal
```
/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
brew install git
brew install node

git clone https://laynef@bitbucket.org/laynef/handlingbrokenjsonformat.git
cd handlingbrokenjsonformat

// node index.js <path-to-your-file>
```

# Run command for checking

Your file can be in any format for that broken json file (not an array type or single object)
It generates a json format that can be used in a developer's codebase
File path given on command run.

```
node index.js <path-to-your-file>
```

